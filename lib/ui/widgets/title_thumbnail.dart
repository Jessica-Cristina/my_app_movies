import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';

class TitleThumbnail extends StatelessWidget {
  final TitleModel title;
  const TitleThumbnail({required this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CustomNetworkImage(
          url: title.posterUrl,
          heigth: 160.0,
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            color: Colors.white38,
            padding: const EdgeInsets.only(left: 8.0, bottom: 4.0, top: 4.0),
            child: const Text(
              '2020',
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
            ),
          ),
        )
      ],
    );
  }
}
