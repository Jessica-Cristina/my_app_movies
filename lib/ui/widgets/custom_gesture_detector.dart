import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/pages/tab_pages/title_detais_page.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class CostumGestureDetector extends StatelessWidget {
  final TitleModel title;
  const CostumGestureDetector({required this.title, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, TitleDetailsPage.name,
            arguments: {'id': title.id, 'isTvShow': title.isTvShow});
      },
      child: GestureDetector(
        child: Container(
          margin: const EdgeInsets.only(left: 10.0),
          color: Colors.teal,
          height: 160.0,
          child: TitleThumbnail(title: title),
        ),
      ),
    );
  }
}
