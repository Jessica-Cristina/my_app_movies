import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';

class ListLikesDeslikes extends StatelessWidget {
  final TitleModel title;
  final bool like;
  const ListLikesDeslikes({required this.like, required this.title, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: CustomNetworkImage(url: title.posterUrl),
          ),
          SizedBox(
            width: 100.0,
            child: Text(
              title.name,
              style: const TextStyle(
                color: Colors.teal,
                fontSize: 15.0,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
          like
              ? IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.thumb_up_alt_rounded))
              : IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.thumb_down_alt_rounded))
        ],
      ),
    );
  }
}
