import 'package:flutter/material.dart';

class CustomNetworkImage extends StatelessWidget {
  final String url;
  final double? heigth;
  const CustomNetworkImage({required this.url, this.heigth, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      url,
      height: heigth,
      fit: BoxFit.contain,
      loadingBuilder: (context, child, loadingprogress) {
        if (loadingprogress == null) {
          return child;
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}
