import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final String label;
  final TextEditingController? controller;
  final IconData? icon;
  final TextInputType? keys;
  final bool obscure;
  final TextInputType? keyBoardType;

  const CustomTextFormField({
    required this.label,
    this.controller,
    this.icon,
    this.keyBoardType,
    this.obscure = false,
    this.keys,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: obscure,
      keyboardType: keyBoardType,
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        border: const OutlineInputBorder(),
        labelText: label,
      ),
    );
  }
}
