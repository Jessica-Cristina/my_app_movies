import 'package:flutter/material.dart';

class TitleCarousel extends StatelessWidget {
  final String label;
  final List<Widget> children;
  const TitleCarousel({required this.children, required this.label, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10.0),
        Text(
          label,
          style: const TextStyle(
              fontSize: 18.0, fontStyle: FontStyle.italic, color: Colors.teal),
        ),
        const SizedBox(height: 5.0),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ],
    );
  }
}
