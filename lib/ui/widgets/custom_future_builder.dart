import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/respositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/custom_gesture_detector.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class CustomFutureBuilder extends StatelessWidget {
  final String titleCarrousel;
  final bool recomends;
  final bool isTvShow;
  final int? titleId;
  final Map<String, dynamic>? params;
  CustomFutureBuilder(this.recomends, this.titleCarrousel, this.isTvShow,
      {this.titleId, Key? key, this.params})
      : super(key: key);

  final _repository = getIt.get<TitleRepositoryInterface>();

  @override
  Widget build(BuildContext context) {
    return recomends ? _getRecommendations(titleId!) : _getTitles();
  }

  Widget _getTitles() {
    return FutureBuilder<List<TitleModel>>(
        future: isTvShow
            ? _repository.getTvList(params: params)
            : _repository.getMoviesList(params: params),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          final movies = snapshot.data;
          return TitleCarousel(
            label: titleCarrousel,
            children:
                movies!.map((t) => CostumGestureDetector(title: t)).toList(),
          );
        });
  }

  Widget _getRecommendations(int titleId) {
    return FutureBuilder<List<TitleModel>>(
        future:
            _repository.getTitleRecommendations(titleId, isTvShow: isTvShow),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          final movies = snapshot.data;
          if (movies!.isEmpty) {
            return const Text("Não há recomendações para esse titulo ainda!");
          } else {
            return TitleCarousel(
              label: titleCarrousel,
              children:
                  movies.map((t) => CostumGestureDetector(title: t)).toList(),
            );
          }
        });
  }
}
