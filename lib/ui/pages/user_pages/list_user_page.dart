import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/respositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/tab_pages/rate_page.dart';

class ListUserPage extends StatelessWidget {
  ListUserPage({Key? key}) : super(key: key);
  static const name = 'list-user-page';

  final _repository = getIt.get<UserRepositoryInterface>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Lista de Usuarios'),
          backgroundColor: Colors.teal,
        ),
        body: FutureBuilder<List<UserModel?>>(
            future: _repository.getUsers(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              final user = snapshot.data;
              return ListView.builder(
                  itemCount: user!.length,
                  itemBuilder: (context, index) {
                    return TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, RatePage.name,
                            arguments: user[index]!.id);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          _buildIconUser(user[index]!.name[0]),
                          const SizedBox(
                            width: 15.0,
                          ),
                          Text(
                            user[index]!.name,
                            style: const TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    );
                  });
            }));
  }

  Widget _buildIconUser(String name) {
    return Container(
      height: 60.0,
      decoration:
          const BoxDecoration(shape: BoxShape.circle, color: Colors.teal),
      child: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            const Icon(
              Icons.circle,
              color: Colors.teal,
              size: 50.0,
            ),
            Text(
              name.toUpperCase(),
              style: const TextStyle(
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}
