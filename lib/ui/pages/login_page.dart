import 'package:flutter/material.dart';
import 'package:my_movies_list/data/exceptions/user_not_found.dart';
import 'package:my_movies_list/data/respositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/sign_page.dart';
import 'package:my_movies_list/ui/widgets/custom_text_form_field.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';

class LoginPage extends StatefulWidget {
  final _userRepository = getIt.get<UserRepositoryInterface>();
  static const name = 'login-page';

  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool processingLogin = false;
  String loginError = '';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                    child: Text(
                      'Informe suas credencias para começar!',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w300,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextFormField(
                    label: 'Email',
                    keyBoardType: TextInputType.emailAddress,
                    icon: Icons.email,
                    controller: _emailController,
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFormField(
                    label: 'Senha',
                    icon: Icons.lock,
                    obscure: true,
                    controller: _passwordController,
                  ),
                  const SizedBox(height: 25.0),
                  Visibility(
                    visible: loginError.isNotEmpty,
                    child: Text(
                      loginError,
                      style: const TextStyle(color: Colors.red),
                    ),
                  ),
                  LoadingButton(
                    isLoading: processingLogin,
                    onPressed: login,
                    child: const Text('Entrar'),
                  ),
                  const SizedBox(height: 15.0),
                  Text(
                    'OU',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, SignPage.name);
                    },
                    child: const Text('Criar minha conta'),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> login() async {
    setState(() {
      processingLogin = true;
      loginError = '';
    });
    try {
      await widget._userRepository
          .login(_emailController.text, _passwordController.text);
      Navigator.pushReplacementNamed(context, HomePage.name);
    } on UserNotFoundException {
      setState(() {
        processingLogin = false;
        loginError = 'Usuário não encontrado';
      });
    } on Exception catch (e) {
      setState(() {
        processingLogin = false;
        loginError = e.toString();
      });
    }
  }
}
