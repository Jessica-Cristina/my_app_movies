import 'package:flutter/material.dart';
import 'package:my_movies_list/data/respositories/user_repository_interface.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/main_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/movies_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/rate_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/series_tab_page.dart';
import 'package:my_movies_list/ui/pages/user_pages/list_user_page.dart';

import '../../locator.dart';
import 'search_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  static const name = 'home-page';

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        drawer: _buidDrawer(context),
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text('Catálogo'),
          bottom: const TabBar(
            tabs: [
              Tab(child: Text('Principal')),
              Tab(child: Text('Filmes')),
              Tab(child: Text('Series'))
            ],
          ),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, SearchPage.name);
              },
              icon: const Icon(Icons.search),
            ),
            IconButton(
              onPressed: () async {
                await getIt.get<UserRepositoryInterface>().clearSession();
                Navigator.pushNamedAndRemoveUntil(
                    context, LoginPage.name, (context) => false);
              },
              icon: const Icon(Icons.exit_to_app),
            ),
          ],
        ),
        body: const TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              MainTabPage(),
              MoviesTabPage(),
              SeriesTabPage(),
            ]),
      ),
    );
  }

  Drawer _buidDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.teal,
            ),
            child: IconButton(
              onPressed: () => Navigator.pushNamed(context, SearchPage.name),
              icon: const Icon(Icons.account_box_rounded),
            ),
          ),
          _buildListTile(
            context: context,
            tile: "Likes : Deslikes",
            ontap: RatePage.name,
          ),
          _buildListTile(
            context: context,
            tile: "Usuarios",
            ontap: ListUserPage.name,
          ),
        ],
      ),
    );
  }

  Widget _buildListTile({
    required BuildContext context,
    required String tile,
    required String ontap,
  }) {
    return ListTile(
      title: Text(tile),
      onTap: () => Navigator.pushNamed(context, ontap),
    );
  }
}
