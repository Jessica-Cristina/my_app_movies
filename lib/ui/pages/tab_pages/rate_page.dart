import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/respositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/list_likes_deslikes.dart';

class RatePage extends StatelessWidget {
  RatePage({Key? key}) : super(key: key);
  static const name = 'hate-page';

  final _repository = getIt.get<TitleRepositoryInterface>();

  @override
  Widget build(BuildContext context) {
    final userId = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: const Text('Likes : Deslikes'),
        ),
        body: FutureBuilder(
          future: userId == null
              ? _repository.getMyRatedList()
              : _repository.getUsersRatedList(userId.toString()),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            final movies = snapshot.data as List;
            return ListView.builder(
                padding: const EdgeInsets.only(top: 20.0),
                itemCount: movies.length,
                itemBuilder: (context, index) {
                  bool like = movies[index].rate == 1 ? true : false;
                  return ListLikesDeslikes(
                    like: like,
                    title: TitleModel(
                        id: movies[index].id,
                        coverUrl: movies[index].coverUrl,
                        name: movies[index].name,
                        posterUrl: movies[index].posterUrl,
                        isTvShow: like),
                  );
                });
          },
        ));
  }
}
