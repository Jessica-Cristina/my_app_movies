import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/respositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/custom_future_builder.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';
import 'package:my_movies_list/ui/widgets/custom_text_form_field.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';

class TitleDetailsPage extends StatefulWidget {
  static const name = 'title-details-page';

  const TitleDetailsPage({Key? key}) : super(key: key);

  @override
  _TitleDetailsPageState createState() => _TitleDetailsPageState();
}

class _TitleDetailsPageState extends State<TitleDetailsPage> {
  final _repository = getIt.get<TitleRepositoryInterface>();
  final _commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as Map;
    int titleId = title['id'] as int;
    bool isTvShow = title['isTvShow'] as bool;

    return FutureBuilder<TitleDetailModel?>(
        future: _repository.getTitleDetails(titleId, isTvShow: isTvShow),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Scaffold(body: Center(child: Text('Carregando ...')));
          }
          final detail = snapshot.data!;
          return Scaffold(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      CustomNetworkImage(
                        url: detail.coverUrl.toString(),
                      ),
                      _buildDetails(detail, context),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildDetails(TitleDetailModel details, BuildContext context) {
    final title = ModalRoute.of(context)!.settings.arguments as Map;
    bool isTvShow = title['isTvShow'] as bool;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          details.name,
          style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 18.0),
        ),
        const SizedBox(height: 20.0),
        Text(
          details.overview,
          style: const TextStyle(fontWeight: FontWeight.w700),
        ),
        const SizedBox(height: 10.0),
        Text('Duração: ' + details.runtime),
        const SizedBox(height: 20.0),
        Row(
          children: details.genres
              .map((g) => Flexible(child: Chip(label: Text(g))))
              .toList(),
        ),
        const SizedBox(height: 20.0),
        FutureBuilder(
          future: _repository.getTitleRate(details.id, isTvShow: isTvShow),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            final rate = snapshot.data;
            if (rate == -1) {
              return _rate(details.id, positivo: false);
            } else if (rate == 1) {
              return _rate(details.id, positivo: true);
            }
            return _rateNull(details.id);
          },
        ),
        const SizedBox(height: 40.0),
        CustomFutureBuilder(
          true,
          "Titulos como esse!",
          isTvShow,
          titleId: details.id,
        ),
        const SizedBox(height: 15.0),
        Row(
          children: [
            Expanded(
              child: CustomTextFormField(
                label: 'Comentar',
                controller: _commentController,
              ),
            ),
            const SizedBox(height: 15.0),
            LoadingButton(
              child: const Text('Enviar'),
              onPressed: () async {
                await _repository.saveComment(
                  details.id,
                  _commentController.text,
                  isTvShow: isTvShow,
                );

                _commentController.clear();
              },
            ),
          ],
        ),
        Column(children: _buildCommentsList(details))
      ],
    );
  }

  List<Widget> _buildCommentsList(TitleDetailModel title) {
    return title.comments
        .map((c) => ListTile(
              title: Text(c.text),
              subtitle: Text(c.date.toString()),
              trailing: IconButton(
                onPressed: () async {
                  bool result = await _repository
                      .removeComment(title.id, c.id, body: {"comment": c.text});
                  result
                      ? setState(() {
                          c;
                        })
                      : ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text("Esse comentario não é seu!")));
                },
                icon: const Icon(Icons.remove),
              ),
            ))
        .toList();
  }

  Widget _rate(int id, {bool? positivo}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: positivo!
          ? [
              IconButton(
                  onPressed: () {
                    setState(() {
                      _saveRate(id, 1);
                    });
                  },
                  icon: const Icon(Icons.thumb_up)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      _saveRate(id, -1);
                    });
                  },
                  icon: const Icon(Icons.thumb_down_alt_outlined)),
            ]
          : [
              IconButton(
                  onPressed: () {
                    setState(() {
                      _saveRate(id, 1);
                    });
                  },
                  icon: const Icon(Icons.thumb_up_alt_outlined)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      _saveRate(id, -1);
                    });
                  },
                  icon: const Icon(Icons.thumb_down)),
            ],
    );
  }

  _saveRate(int id, int i) async {
    return await _repository.saveTitleRate(id, i);
  }

  Widget _rateNull(int id) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      IconButton(
          onPressed: () {
            setState(() {
              _saveRate(id, 1);
            });
          },
          icon: const Icon(Icons.thumb_up_alt_outlined)),
      IconButton(
          onPressed: () {
            setState(() {
              _saveRate(id, -1);
            });
          },
          icon: const Icon(Icons.thumb_down_alt_outlined)),
    ]);
  }
}
