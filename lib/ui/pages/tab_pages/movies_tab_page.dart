import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/widgets/custom_future_builder.dart';

class MoviesTabPage extends StatelessWidget {
  const MoviesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Column(
              children: [
                CustomFutureBuilder(false, 'Filmes de Ação', false,
                    params: const {'genre': 28}),
                CustomFutureBuilder(false, 'Filmes Lançamentos', false,
                    params: const {'genre': 16}),
                CustomFutureBuilder(false, 'Fimes Dramaticos', false,
                    params: const {'genre': 18}),
                CustomFutureBuilder(false, 'Filmes de Crime', false,
                    params: const {'genre': 80}),
                CustomFutureBuilder(false, 'Filmes de Fantasia', false,
                    params: const {'genre': 14}),
                CustomFutureBuilder(false, 'Filmes de Thriller', false,
                    params: const {'genre': 53}),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
