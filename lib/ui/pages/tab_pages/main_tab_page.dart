import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/widgets/custom_future_builder.dart';

class MainTabPage extends StatelessWidget {
  const MainTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        children: [
          Column(
            children: [
              CustomFutureBuilder(false, 'Filmes de Ação', false,
                  params: const {'genre': 28}),
              CustomFutureBuilder(false, 'Filmes Lançamentos', false,
                  params: const {'genre': 16}),
              CustomFutureBuilder(false, 'Series Lançamentos', true,
                  params: const {'genre': 18}),
              CustomFutureBuilder(false, 'Filmes de Ficção Cientifica', false,
                  params: const {'genre': 80}),
              CustomFutureBuilder(false, 'Series de Fantasia', true,
                  params: const {'genre': 80}),
              CustomFutureBuilder(false, 'Filmes de Aventura', false,
                  params: const {'genre': 12}),
            ],
          ),
        ],
      ),
    );
  }
}
