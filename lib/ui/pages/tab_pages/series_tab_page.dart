import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/widgets/custom_future_builder.dart';

class SeriesTabPage extends StatelessWidget {
  const SeriesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Column(
              children: [
                CustomFutureBuilder(false, 'Series de Mistério', true,
                    params: const {'genre': 9648}),
                CustomFutureBuilder(false, 'Series de Drama', true,
                    params: const {'genre': 18}),
                CustomFutureBuilder(false, 'Series de Crimes', true,
                    params: const {'genre': 80}),
                CustomFutureBuilder(false, 'Filmes de Ficção Cientifica', true,
                    params: const {'genre': 9648}),
                CustomFutureBuilder(false, 'Filmes de Fantasia', true,
                    params: const {'genre': 18}),
                CustomFutureBuilder(false, 'Filmes de Aventura', true,
                    params: const {'genre': 80}),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
