import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);
  //final _repository = TitleRepository();
  static const name = 'search-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: TextFormField(
          style: const TextStyle(color: Colors.white),
          decoration: const InputDecoration(
            hintText: 'Pesquise aqui ...',
            hintStyle: TextStyle(color: Colors.white30),
            border: InputBorder.none,
          ),
        ),
      ),
      body: GridView.count(
        padding: const EdgeInsets.only(top: 8.0),
        mainAxisSpacing: 20.0,
        crossAxisCount: 2,
        children: const [],
      ),
    );
  }
}
