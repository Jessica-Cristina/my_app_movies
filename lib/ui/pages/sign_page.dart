import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_movies_list/data/respositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/widgets/custom_text_form_field.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';

class SignPage extends StatefulWidget {
  final _userRepository = getIt.get<UserRepositoryInterface>();
  static const name = 'sign-page';

  SignPage({Key? key}) : super(key: key);

  @override
  _SignPageState createState() => _SignPageState();
}

class _SignPageState extends State<SignPage> {
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool processingLogin = false;
  String loginError = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 70.0),
            const Text(
              'Informe seus dados para o cadastro!',
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w300,
                  color: Colors.teal),
            ),
            const SizedBox(height: 25.0),
            CustomTextFormField(
              obscure: false,
              label: 'Nome',
              controller: _nameController,
            ),
            const SizedBox(
              height: 20.0,
            ),
            CustomTextFormField(
              label: 'Email',
              keyBoardType: TextInputType.emailAddress,
              icon: Icons.email,
              controller: _emailController,
            ),
            const SizedBox(
              height: 20.0,
            ),
            CustomTextFormField(
              label: 'Senha',
              obscure: true,
              controller: _passwordController,
            ),
            const SizedBox(
              height: 25.0,
            ),
            LoadingButton(
              isLoading: processingLogin,
              onPressed: signup,
              child: const Text('Cadastrar'),
            ),
            const SizedBox(height: 10.0),
            TextButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, LoginPage.name);
              },
              child: const Text(
                'Voltar',
                style: TextStyle(color: Colors.teal),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> signup() async {
    setState(() {
      processingLogin = true;
      loginError = '';
    });
    try {
      await widget._userRepository.register(_emailController.text,
          _passwordController.text, _nameController.text);
      Navigator.pushReplacementNamed(context, LoginPage.name);
    } on Exception {
      setState(() {
        processingLogin = false;
        loginError = 'Usuário não cadastrado';
      });
    }
  }
}
