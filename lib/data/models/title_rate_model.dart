class TitleRateModel {
  int id;
  String name;
  String originalName;
  String coverUrl;
  String posterUrl;
  int rate;

  TitleRateModel(
      {required this.id,
      required this.name,
      required this.originalName,
      required this.coverUrl,
      required this.posterUrl,
      required this.rate});

  factory TitleRateModel.fromJson(Map<String, dynamic> json) {
    return TitleRateModel(
        id: json['id'],
        name: json['name'],
        coverUrl: json['cover_url'],
        posterUrl: json['poster_url'],
        originalName: json['original_name'],
        rate: json['rate']);
  }
}
