import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/models/title_rate_model.dart';

abstract class TitleRepositoryInterface {
  Future<List<TitleModel>> getMoviesList({Map<String, dynamic>? params});
  Future<TitleDetailModel?> getTitleDetails(int id, {bool isTvShow = false});
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params});
  Future<List<TitleModel>> getTitleRecommendations(int id,
      {bool isTvShow = false});
  Future<bool> saveComment(int titleId, String text, {bool isTvShow = false});
  Future<bool> removeComment(
    int titleId,
    int commentId, {
    bool isTvShow = false,
    Map<String, dynamic>? params,
    Map<String, dynamic>? body,
  });
  Future<bool> saveTitleRate(int titleId, int rate, {bool isTvShow = false});
  Future<int> getTitleRate(int titleId, {bool isTvShow = false});
  Future<List<TitleRateModel>> getUsersRatedList(String id);
  Future<List<TitleRateModel>> getMyRatedList();
}
