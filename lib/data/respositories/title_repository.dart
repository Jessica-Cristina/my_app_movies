import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_rate_model.dart';
import 'package:my_movies_list/data/respositories/title_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';

class TitleRepository implements TitleRepositoryInterface {
  final HttpService _httpService;
  final _secureStorage = const FlutterSecureStorage();

  final _baseUrl = 'https://xbfuvqstcb.execute-api.us-east-1.amazonaws.com/dev';

  TitleRepository(this._httpService);

  @override
  Future<List<TitleModel>> getMoviesList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/movies';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<TitleDetailModel?> getTitleDetails(int id,
      {bool isTvShow = false}) async {
    final type = isTvShow ? 'tv' : 'movies';
    String uri = '$_baseUrl/$type/$id';
    var response = await _httpService.getRequest(uri);
    if (response.success) {
      return TitleDetailModel.fromJson(response.content!);
    }
    return TitleDetailModel(
        id: 0,
        name: 'Titulo Não Encontrado',
        overview: "Sinpse:",
        releaseDate: null,
        voteAverage: 0,
        homePage: "Home Page:",
        runtime: "Time",
        genres: ['genres'],
        coverUrl:
            'https://apps.todocartoes.com.br/assets/default-c430dc3f6bf8734aa34831c5394c3fae0ac302a036587b85b90fb4a4dd22f0fd.png',
        posterUrl:
            'https://apps.todocartoes.com.br/assets/default-c430dc3f6bf8734aa34831c5394c3fae0ac302a036587b85b90fb4a4dd22f0fd.png',
        comments: [CommentModel(id: 0, text: 'text', date: DateTime.now())]);
  }

  @override
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/tv';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTitleRecommendations(int id,
      {bool isTvShow = false}) async {
    final type = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$type/$id/recommendations';

    var response = await _httpService.getRequest(uri);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<bool> saveComment(int titleId, String text,
      {bool isTvShow = false}) async {
    final type = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$type/$titleId/comment';
    final token = await _secureStorage.read(key: 'AUTH_TOKEN') as String;
    final headers = {'Authorization': 'Bearer $token'};
    final result = await _httpService.postRequest(uri, {'comment': text},
        headers: headers);

    return result.success;
  }

  @override
  Future<int> getTitleRate(int titleId, {bool isTvShow = false}) async {
    final type = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$type/$titleId/rate';
    final token = await _secureStorage.read(key: 'AUTH_TOKEN') as String;
    final headers = {'Authorization': 'Bearer $token'};
    var response = await _httpService.getRequest(uri, headers: headers);
    var result = 0;
    if (response.success) {
      result = response.content!['rate'] as int;
      return result;
    }

    return result;
  }

  @override
  Future<bool> saveTitleRate(int titleId, int rate,
      {bool isTvShow = false}) async {
    final type = isTvShow ? 'tv' : 'movies';
    final token = await _secureStorage.read(key: 'AUTH_TOKEN') as String;
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/$type/$titleId/rate';
    final result =
        await _httpService.postRequest(uri, {'rate': rate}, headers: headers);

    return result.success;
  }

  @override
  Future<List<TitleRateModel>> getUsersRatedList(String id) async {
    final uri = '$_baseUrl/users/$id/titles-rated';
    final token = await _secureStorage.read(key: 'AUTH_TOKEN') as String;
    final headers = {'Authorization': 'Bearer $token'};
    var response = await _httpService.getRequest(uri, headers: headers);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleRateModel>.from(
          data.map((j) => TitleRateModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<List<TitleRateModel>> getMyRatedList() async {
    final uri = '$_baseUrl/users/titles-rated';
    final token = await _secureStorage.read(key: 'AUTH_TOKEN') as String;
    final headers = {'Authorization': 'Bearer $token'};
    var response = await _httpService.getRequest(uri, headers: headers);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleRateModel>.from(
          data.map((j) => TitleRateModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<bool> removeComment(int titleId, int commentId,
      {bool isTvShow = false,
      Map<String, dynamic>? params,
      Map<String, dynamic>? body}) async {
    final type = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$type/$titleId/$commentId/comment';
    final token = await _secureStorage.read(key: 'AUTH_TOKEN') as String;
    final headers = {'Authorization': 'Bearer $token'};
    final result = await _httpService.deleteRequest(uri,
        headers: headers, params: params, body: body);

    return result.success;
  }
}
