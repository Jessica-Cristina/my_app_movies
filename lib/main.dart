import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/sign_page.dart';
import 'package:my_movies_list/ui/pages/splash_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/rate_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/title_detais_page.dart';
import 'package:my_movies_list/ui/pages/user_pages/list_user_page.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Movies',
      debugShowCheckedModeBanner: false,
      home: SplashPage(),
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => LoginPage(),
        SignPage.name: (_) => SignPage(),
        HomePage.name: (_) => const HomePage(),
        RatePage.name: (_) => RatePage(),
        SearchPage.name: (_) => const SearchPage(),
        ListUserPage.name: (_) => ListUserPage(),
        TitleDetailsPage.name: (_) => TitleDetailsPage(),
      },
    );
  }
}
